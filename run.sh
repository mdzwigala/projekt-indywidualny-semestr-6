#!/bin/bash
cd ./server
composer install
docker-compose up -d
php bin/console d:m:m
cd ../react
npm install
yarn start
