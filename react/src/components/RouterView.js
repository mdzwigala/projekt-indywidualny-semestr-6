import React from 'react';
import {Route} from "react-router-dom";
import Index from "./pages/Index";
import Login from "./pages/Login";
import MakeReservation from "./pages/MakeReservation";
import Container from "react-bootstrap/Container";
import ListApartments from "./pages/ListApartments";
import CreateApartment from './pages/CreateApartment';

class RouterView extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Container style={{marginTop: '15px'}}>
                    <Route path="/" exact component={ListApartments}/>
                    <Route path="/login" component={Login} />
                    <Route path="/reserve/:id" component={MakeReservation} />
                    <Route path="/apartments" component={ListApartments}/>
                    <Route path="/addApartment" component={CreateApartment}/>
                </Container>
            </React.Fragment>
        )
    }
}

export default RouterView;
