import Button from '@material-ui/core/Button';
import React from "react";

function DrawerButton(props) {
    return <Button
        variant={"contained"}
        fullWidth={true}
        color={"primary"}
        size={"large"}
        style={{marginTop: '3px'}}
        {...props}
    >{props.children}</Button>;
}

export default DrawerButton;
