import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import {connect} from 'react-redux';
import store from '@core/store/store';
import DrawerButton from '@components/buttons/DrawerButton';
import {Route} from 'react-router-dom';

const mapStateToProps = (state) => (
    {
        open: state.drawerOpen
    }
);

class AppDrawer extends React.Component {

    goToRoute = (history, route) => {
        history.push(route);
        store.dispatch({type: 'TOGGLE_DRAWER'})
    };
    render() {
        return (
            <Drawer anchor={"top"} open={this.props.open} variant={"temporary"}>
                <div style={{marginTop: '5px', paddingLeft: '5px', paddingRight: '5px'}}>
                    <DrawerButton onClick={(e) => store.dispatch({type: 'TOGGLE_DRAWER'})}>
                        Zamknij Menu
                    </DrawerButton>
                    <br/>
                    <Route render={({history}) => {
                        return (
                            <React.Fragment>
                                <DrawerButton onClick={() => this.goToRoute(history, '/apartments')}>
                                    Lista apartamentów
                                </DrawerButton>
                                <DrawerButton onClick={() => this.goToRoute(history, '/apartments')}>Moje apartamenty</DrawerButton>
                                <DrawerButton onClick={() => this.goToRoute(history, '/search')}>Wyszukaj apartament</DrawerButton>
                                <DrawerButton onClick={() => this.goToRoute(history, '/reservations')}>Moje rezerwacje </DrawerButton>
                                <DrawerButton onClick={() => this.goToRoute(history, '/addApartment')}>Dodaj apartament</DrawerButton>
                            </React.Fragment>
                        )
                    }} />
                </div>
            </Drawer>
        )
    }
}

export default connect(mapStateToProps)(AppDrawer);
