import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {Link} from 'react-router-dom';
import store from '@core/store/store';
import {connect} from "react-redux";
import {Route} from 'react-router-dom';

const loginStyle = {
    'cursor': 'pointer',
    'textDecoration': 'none',
    'color': 'white'
};

class NavBar extends React.Component {
    toggleDrawer = () => {
        store.dispatch({
            type: 'TOGGLE_DRAWER'
        });
    };
    render() {
        return (
            <AppBar position="static" color={"primary"}>
                <Toolbar>
                    <IconButton onClick={this.toggleDrawer} color="inherit" aria-label="Menu">
                        <MenuIcon/>
                    </IconButton>
                    <Link to="/" style={loginStyle}>
                        <Typography variant="h6" color="inherit">
                            Wynajem Apartamentów - Projekt Indywidualny
                        </Typography>
                    </Link>
                        <Link to="/login">
                            <Button size={"large"}  color="secondary">Login</Button>
                        </Link>
                </Toolbar>
            </AppBar>
        )
    }
}

export default connect()(NavBar)
