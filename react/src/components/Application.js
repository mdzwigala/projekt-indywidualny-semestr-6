import React from 'react';
import './../App.css';
import NavBar from '@components/layout/NavBar';
import RouterView from '@components/RouterView';
import Footer from '@components/layout/Footer';
// import Drawer from '@material-ui/core/Drawer';
import AppDrawer from './layout/AppDrawer';
import {Provider} from 'react-redux';
import store from '../core/store/store';
import {BrowserRouter as Router} from "react-router-dom";

class Application extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <div className="App">
                        <NavBar/>
                        <AppDrawer/>
                        <RouterView/>
                        {/*<Footer/>*/}
                    </div>
                </Router>
            </Provider>
        );
    }
}

export default Application;
