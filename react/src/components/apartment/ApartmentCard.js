import * as React from "react";
import {Card} from "@material-ui/core";
import Image from "react-bootstrap/Image";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";


class ApartmentCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.id,
            imgSrc: props.imgSrc,
            description: props.description,
            price: props.price,
            name: props.name,
            city: props.city
        };
    }

    render() {
        return <Card style={{cursor: 'pointer', marginBottom: '5px'}} {...this.props}>
            <Row>
                <Col md={6}>
                    <h2>{this.state.city}</h2>
                    <Image
                        style={{marginTop: '5px', marginBottom: '5px'}}
                        rounded={true}
                        height={'150px'}
                        src={'https://s-ec.bstatic.com/images/hotel/max1024x768/735/73580604.jpg'}/>
                </Col>
                <Col md={6}>
                    <h3>{this.state.name}</h3>
                    <p>{this.state.description}</p>
                </Col>
            </Row>

        </Card>
    }

}

export default ApartmentCard;
