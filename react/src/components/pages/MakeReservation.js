import * as React from "react";
import {Button, Card, FormLabel, Input, TextField} from "@material-ui/core";
import moment from 'moment';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "react-bootstrap/Image";
import ApiClient from "../../libraries/ApiClient";

class MakeReservation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pricePerDay: 25,
            dateFrom: '2019-01-01',
            dateTo: '2019-01-01',
            price: 0,
            id: this.props.match.params.id,
            name: '',
            city: '',
            description: ''
        };
        this.client = new ApiClient();
        console.log(this.props.match);
    }
    componentDidMount() {
        this.calculatePrice();
        this.client.fetchApartment(this.state.id).then((response) => {
            this.setState({
                name: response.data.name,
                city: response.data.city,
                description: response.data.description
            });
        })
    }

    calculatePrice = () => {
        this.setState({
            price: this.getDiffBetweenDates(this.state.dateFrom, this.state.dateTo) * this.state.pricePerDay
        });
    };
    handleDateFromChange = (e) => {
        if (this.getDiffBetweenDates(e.target.value, this.state.dateTo) < 0) {
            alert('Data do musi być po dacie od!');
            return;
        }
        this.setState({
            dateFrom: e.target.value
        }, () => {
            this.calculatePrice();
        });

    };
    handleDateToChange = (e) => {
        if (this.getDiffBetweenDates(this.state.dateFrom, e.target.value) < 0) {
            alert('Data do musi być po dacie od!');
            return;
        }
        this.setState({
            dateTo: e.target.value
        }, () => {
            this.calculatePrice();
        });

    };

    getDiffBetweenDates(dateFrom, dateTo) {
        return moment(dateTo, 'YYYY-MM-DD').diff(moment(dateFrom, 'YYYY-MM-DD'), 'days');
    }

    render() {
        return <Card>
            <Row>
                <Card>

                </Card>
                <Col md={6}>
                    <Image
                        rounded={true}
                        style={{maxWidth: '100%'}}
                        src={'https://s-ec.bstatic.com/images/hotel/max1024x768/735/73580604.jpg'}
                    />
                </Col>
                <Col md={6}>
                    <h2>Wynajmij apartament {this.state.name} w mieście {this.state.city}</h2>
                    <h3>Cena za pobyt: {this.state.price + ' PLN'}</h3>
                    <p>Opis: {this.state.description}</p>
                    <Row style={{paddingTop: '25%'}}>
                        <Col md={12}>
                            <Row>
                                <Col md={12}>
                                    <FormLabel>
                                        Data wynajmu od
                                    </FormLabel>
                                    <Input
                                        value={this.state.dateFrom}
                                        type={"date"} onChange={this.handleDateFromChange}/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12}>
                                    <FormLabel>
                                        Data wynajmu do
                                    </FormLabel>
                                    <Input value={this.state.dateTo} onChange={this.handleDateToChange} type={"date"}/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12}>
                                    <Button style={{marginTop:'10px'}} color={"primary"} onClick={(e) => alert('Próba rezerwacji apartamentu')}
                                            variant={"contained"}>Zarezerwuj</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Card>

    }
}

export default MakeReservation;
