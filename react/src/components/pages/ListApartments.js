import * as React from "react";
import ApartmentCard from "../apartment/ApartmentCard";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ApiClient from '@libraries/ApiClient'

import {Route} from 'react-router-dom';

class ListApartments extends React.Component {
    constructor(props) {
        super(props);
        this.client = new ApiClient();
        this.state = {
            apartments: []
        };
    }
    fetchApartments = () => {
        this.client.fetchApartaments().then((response) => {
            this.setState({
                apartments: response.data['hydra:member']
            });
        });
    };
    componentDidMount() {
        this.fetchApartments();
    }
    render() {
        let list = this.state.apartments.map((ap) => {
            return (<Col md={12}>
                <Route render={({history}) => {
                    return (<ApartmentCard name={ap.name} city={ap.city} id={ap.id} description={ap.description} imgSrc={ap.imgSrc} onClick={() => {history.push(`/reserve/${ap.id}`)}} />)
                }}/>
            </Col>)
        });
        return (<Row>
            {list}
        </Row>)
    }
}

export default ListApartments;
