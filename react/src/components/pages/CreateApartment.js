import * as React from "react";
import ApartmentCard from "../apartment/ApartmentCard";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ApiClient from '@libraries/ApiClient'
import {Route} from 'react-router-dom';
import {Redirect} from 'react-router';
import {Button, TextField} from "@material-ui/core";

class CreateApartment extends React.Component {
    constructor(props) {
        super(props);
        this.client = new ApiClient();
        this.state = {
            name: '',
            city: '',
            description: '',
            redirect: false
        };
    }
    createApartment = () => {
        this.client.createApartment({
            name: this.state.name,
            city: this.state.city,
            description: this.state.description
        }).then(() => {
            alert('Dodano apartament!');
            this.setState({
                redirect: true
            });
        });
    };
    handleFormChange = (e) => {
        let state = {};
        state[e.target.name] = e.target.value;
        this.setState(state);
    };
    render() {
        if (this.state.redirect) {
            return <Redirect to="/apartments" />
        }
        return (
            <Row>
                <Col md={12}>
                    <div>
                        <TextField onChange={this.handleFormChange} value={this.state.name} name={"name"} label={"Nazwa"}/>
                        <TextField onChange={this.handleFormChange} value={this.state.city} name={"city"} label={"Miasto"}/>
                        <TextField onChange={this.handleFormChange} value={this.state.description} name={"description"} label={"Opis"}/>
                    </div>
                    <div>
                        <Button onClick={this.createApartment}  style={{marginTop: '10px'}}>Dodaj apartament</Button>
                    </div>
                </Col>
            </Row>
        )
    }
}

export default CreateApartment;
