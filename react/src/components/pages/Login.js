import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import ApiClient from '@libraries/ApiClient';
import store from '@core/store/store';
import {ADD_TOKEN} from "../../core/store/actions";

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.client = new ApiClient();
        this.state = {
            login: null,
            password: null
        };
    }
    login = async () => {
        let response = await this.client.login(this.state.login, this.state.password);
        store.dispatch({
            type: ADD_TOKEN,
            jwtToken: response.jwtToken
        });
        console.log(response);
    };
    render() {
        return (
            <div>
                <h1>Zaloguj się</h1>
                <TextField
                    id="login"
                    label="Login"
                    onChange={e => this.setState({login: e.target.value})}
                />
                <br/>
                <TextField
                    id="password"
                    label="Hasło"
                    type={"password"}
                    onChange={e => this.setState({password: e.target.value})}
                />
                <br/>
                <Button
                    style={{
                        "marginTop": "15px"
                    }}
                    variant={"contained"}
                    size={"large"}
                    onClick={this.login}
                    color={"primary"}>
                    Zaloguj się
                </Button>
            </div>
        )
    }
}

export default Login;
