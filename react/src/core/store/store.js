import {createStore} from 'redux';
import {ADD_TOKEN, TOGGLE_DRAWER} from './actions';
import state from './state';

const reducer = (state, action) => {
    switch (action.type) {
        case TOGGLE_DRAWER:
            return {...state, drawerOpen: !state.drawerOpen};
        case 'DISABLE_DRAWER':
            return {...state, drawerOpen: false};
        case ADD_TOKEN:
            return {...state, jwtToken: action.token};
        default:
            return state;
    }
};

const store = createStore(reducer, state);

export default store;
