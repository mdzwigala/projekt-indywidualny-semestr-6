const axios = require('axios');
const BASE_URL = 'http://localhost:8000/api';

class ApiClient {
    async login(login, password) {
        return login + ':' + password;
    };
    async register(login, password) {
        return login + ':' + password;
    };

    async fetchApartaments() {
        let data = [];
        return axios.get(BASE_URL + '/apartments');
    };

    async fetchApartment(id) {
        return axios.get(BASE_URL + '/apartments/' + id);
    }

    async fetchMyApartments() {
        return this.fetchApartaments()
    }

    async createApartment(data) {
        return axios.post(BASE_URL + '/apartments', data);
    };

    async deleteApartment(id) {
        return new Promise({});
    };

    async createApartmentConfiguration({apartmentId, dateFrom, dateTo, price}) {
        return new Promise({});
    }

    async makeReservation(apartmentId, dateFrom, dateTo) {
        return new Promise({});
    }
}


export default ApiClient;
